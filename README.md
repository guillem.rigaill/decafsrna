# DecafsRNA

R codes to run Decafs on the B. Subtilis analysis RNA dataset of P. Nicolas et al. 2009

# Run the Bacillus Subtilis rna analysis of Romano et al. 2020

The main script is Figure_For_Paper_Review1.R.
It runs :
 - Collect_Data.R to retrieve the RNA data.
 - get_HMM_Score.R to get the score as in Nicolas et al. 2009
 - Run_Segmentation_review1.R to run Decafs with various penalty levels and compute $M(delta)$ for various R(delta) levels.
The output of Decafs are stored in the directory saveRData_rev1

The S100path... directory, kindly provided by Pierre Nicolas, contain the output of hmmTiling 
The probesannot.dat file, kindly provided by Pierre Nicolas, contains the annotation of the probes.

