rm(list=ls())
#devtools::install_github("gtromano/DeCAFS")
library(DeCAFS)
library(parallel)

nmax <- 300000 ## collect all data, first 300 000 probes of Plus and Minus
source("Collect_Data.R")
source("Get_HMM_Score.R")
howclose <- 22 ## as in Pierre Nicolas et al. paper (distance to the known promorter or terminator)
column <- "X"
## first run segmentations and store results in RData files
source("Run_Segmentations_review1.R")


## 
## betas <- seq(2, 20, by=1) ## already set in Run_Segmentations.R
saveAll <- list()
for(beta in betas){


BothStrand <- list()
for(strand in c("Minus", "Plus")){
    ## GET DATA AND OUTPUT OF THE HMM
    mydata      <- listData[[strand]]$mydata; 
    mydata$gDNA <- mydata$X - mydata$XrX
    mydatand    <- listData[[strand]]$mydatand
    mydatano    <- listData[[strand]]$mydatano
    probesannot <- listData[[strand]]$probesannot

 
    y <- mydata[[column]];
    n <- length(y)
   

    mydata$prom <- abs(probesannot$dprom) <= howclose
    mydata$term <- abs(probesannot$dterm) <= howclose


    file.y <- paste0("saveRData_rev1/y_", strand ,"_beta=", beta, "_default.RData")
    load(file.y)
    rm(file.y)

   

    allRes <- list() 
    allRes$decafs <- diff(c(res.y$signal)) 
    ### NOTE: ONE COULD ALSO LOOK AT THE RESULTS OF hmmTiling and and ano 
    ### replacing mydata by mydatano and mydatand
    allRes$hmm    <- diff(mydatand$ES) #diff(mydatano$ES)[-n]

    allRes$diff   <- diff(y)
    allRes$prom <- mydata$prom[-n]
    allRes$term <- mydata$term[-n]

    hmm.ori <- getScoreHMM.up_scoreAndFeat(mydata, probesannot)
    allRes$hmmU.ori <- hmm.ori$jump
    allRes$hmmU.ori.feat <- hmm.ori$feat
   
    hmm.no <- getScoreHMM.up_scoreAndFeat(mydatano, probesannot)
    allRes$hmmUno.ori <- hmm.no$jump
    allRes$hmmUno.ori.feat <- hmm.no$feat

    hmm.ori <- getScoreHMM.dw_scoreAndFeat(mydata, probesannot)
    allRes$hmmD.ori <- hmm.ori$jump
    allRes$hmmD.ori.feat <- hmm.ori$feat


    hmm.no <- getScoreHMM.dw_scoreAndFeat(mydatano, probesannot)
    allRes$hmmDno.ori <- hmm.no$jump
    allRes$hmmDno.ori.feat <- hmm.no$feat
    
    ## get curves for plots
    mRes <- allRes
    out <- list()
    out$promoters.info <- list(
    decafs     = mRes$prom[order(mRes$decafs, decreasing = T)],
    hmm        = mRes$prom[order(mRes$hmm, decreasing = T)],
    diff       = mRes$prom[order(mRes$diff, decreasing = T)],
    hmm.ori    = mRes$hmmU.ori.feat[order(mRes$hmmU.ori, decreasing = T)],
    hmm.no    =  mRes$hmmUno.ori.feat[order(mRes$hmmUno.ori, decreasing = T)]
    )

    out$terminators.info <- list(
    decafs     = mRes$term[order(mRes$decafs, decreasing = F)],
    hmm        = mRes$term[order(mRes$hmm, decreasing = F)],
    diff       = mRes$term[order(mRes$diff, decreasing = F)],
    hmm.ori    = mRes$hmmD.ori.feat[order(mRes$hmmD.ori, decreasing = T)],
    hmm.no    =  mRes$hmmDno.ori.feat[order(mRes$hmmDno.ori, decreasing = T)]
    )

    BothStrand[[strand]] <- out

}

saveAll[[beta]] <- BothStrand
}


getScore <- function(info, nbpred){
   topredict <- sum(info$hmm)
   n <- length(info$hmm)
   data.frame(decafs = cumsum(info$decafs)[nbpred], 
   hmm    = cumsum(info$hmm)[nbpred],
   hmm.ori = cumsum(info$hmm.ori)[nbpred]) 
   
}

TPR <- list()
nbPreds <- c(250, 500, 750, 1000, 1500, 2000, 3000, 4000)
for(firstPred in nbPreds){
TPR[[firstPred]] <- do.call(rbind, lapply(betas, FUN=function(beta){
  dat <- cbind(
  getScore(saveAll[[beta]][["Minus"]]$promoters.info, firstPred),
  getScore(saveAll[[beta]][["Plus"]]$promoters.info, firstPred),
  getScore(saveAll[[beta]][["Minus"]]$terminators.info, firstPred),
  getScore(saveAll[[beta]][["Plus"]]$terminators.info, firstPred))
  colnames(dat) <- paste0(rep(c("MinusProm.", "PlusProm.", "MinusTerm.", "PlusTerm."), each=2), colnames(dat))
  dat

}
))
}

## black is minus strand
## red is plus strand
## plain is decafs
## dotted  is hmmTiling

pdf("img/7-TPR_as_a_function_of_beta.pdf", width=9, height=12)
par(mfrow=c(4, length(nbPreds)/2))
for(firstPred in nbPreds){
matplot(betas, TPR[[firstPred]][, 1:6], type="l", lty=c(1, 3, 2), col=rep(c(1:2), each=3, lwd=c(1, 3, 1)), 
		main=paste("Prom. R =", firstPred), ylab="TP")}#; abline(v=c(5, 10), lty=3, col="blue")}


for(firstPred in nbPreds){
matplot(betas, TPR[[firstPred]][, 1:6+6], type="l", lty=c(1, 3, 2), col=rep(c(1:2), each=3, lwd=c(1, 3, 1)), 
		main=paste("Term. R =", firstPred), ylab="TP")}#; abline(v=c(5, 10), lty=3, col="blue")}
dev.off()

beta.cv <- betas[apply(TPR[[750]], 2, which.max)][c(1, 4, 7, 10)]
beta.name <- c("Prom.M", "Prom.P", "Term.M", "Term.P")


###############################################################
### reproduce figure from hmmtiling
###############################################################
### Prom Min -> Plus
promoters.info <- saveAll[[beta.cv[1]]]$Plus$promoters.info
png("img/7-Prom_LearnMinus_TestPlus.png", width=1000, height=1000)
par(mar=c(4.5,4.5,1,1))

plot(0,0,type="n",xlim=c(0,4000),ylim=c(0,1250),xlab="number of breakpoints", ylab="matches to predicted promoters",
     axes=F,cex.lab=2)
axis(1,at=seq(0,4000,by=1000),cex.axis=1.6,lwd=3)
axis(2,at=seq(0,1250,by=250),cex.axis=1.6,lwd=3)

lines(cumsum(promoters.info$decafs), col="red",lty=1, lwd=4)
lines(cumsum(promoters.info$hmm), col="black",lty=2, lwd=4)
lines(cumsum(promoters.info$hmm.ori), col="black",lty=1, lwd=4)
lines(c(0, 8000), c(0, 8000), lty=3, lwd=1, col="darkgreen")
legend("topleft", lty=c(1, 2, 1), col=c("red", "black", "black"), legend=c("DeCAFS", "hmmTiling.all", "hmmTiling.ori"), lwd=2, cex=4) 
dev.off()

####################################################################
### Prom Plus -> Minus
promoters.info <- saveAll[[beta.cv[2]]]$Minus$promoters.info
png("img/7-Prom_LearnPlus_TestMinus.png", width=1000, height=1000)
par(mar=c(4.5,4.5,1,1))

plot(0,0,type="n",xlim=c(0,4000),ylim=c(0,1250),xlab="number of breakpoints", ylab="matches to predicted promoters",
     axes=F,cex.lab=2)
axis(1,at=seq(0,4000,by=1000),cex.axis=1.6,lwd=3)
axis(2,at=seq(0,1250,by=250),cex.axis=1.6,lwd=3)

lines(cumsum(promoters.info$decafs), col="red",lty=1, lwd=4)
lines(cumsum(promoters.info$hmm), col="black",lty=2, lwd=4)
lines(cumsum(promoters.info$hmm.ori), col="black",lty=1, lwd=4)
lines(c(0, 8000), c(0, 8000), lty=3, lwd=1, col="darkgreen")
legend("topleft", lty=c(1, 2, 1), col=c("red", "black", "black"), legend=c("DeCAFS", "hmmTiling.all", "hmmTiling.ori"), lwd=2, cex=4) 
dev.off()

###################################################################
### Term Minus -> Plus 
terminators.info <- saveAll[[beta.cv[3]]]$Plus$terminators.info
png("img/7-Term_LearnMinus_TestPlus.png", width=1000, height=1000)
par(mar=c(4.5,4.5,1,1))

plot(0,0,type="n",xlim=c(0,4000),ylim=c(0,1250),xlab="number of breakpoints", ylab="matches to predicted terminators",
     axes=F,cex.lab=2)
axis(1,at=seq(0,4000,by=1000),cex.axis=1.6,lwd=3)
axis(2,at=seq(0,1250,by=250),cex.axis=1.6,lwd=3)

lines(cumsum(terminators.info$decafs), col="red",lty=1, lwd=4)
lines(cumsum(terminators.info$hmm), col="black",lty=2, lwd=4)
lines(cumsum(terminators.info$hmm.ori), col="black",lty=1, lwd=4)
#lines(cumsum(terminators.info$hmm.no), col="orange",lty=3, lwd=4)
#lines(cumsum(terminators.info$diff), col="blue",lty=3, lwd=4)
lines(c(0, 8000), c(0, 8000), lty=3, lwd=1, col="darkgreen")
#lines(c(0, 6000), c(1000, 1000), lty=3, lwd=1, col="darkgreen")
#lines(c(6000, 6000), c(0, 1000), lty=3, lwd=1, col="darkgreen")
#legend("bottomright", lty=c(1, 2, 1, 3, 3), col=c("red", "black", "black", "orange", "blue"), legend=c("decafs", "hmmTiling.all", "hmmTiling.ori", "hmmTiling.no", "raw.diff"), lwd=2, cex=4)
legend("topleft", lty=c(1, 2, 1), col=c("red", "black", "black"), legend=c("DeCAFS", "hmmTiling.all", "hmmTiling.ori"), lwd=2, cex=4) #, "hmmTiling.no", "orange"
dev.off()


###################################################################
### Term Plus -> Minus
terminators.info <- saveAll[[beta.cv[4]]]$Minus$terminators.info
png("img/7-Term_LearnPlus_TestMinus.png", width=1000, height=1000)
par(mar=c(4.5,4.5,1,1))

plot(0,0,type="n",xlim=c(0,4000),ylim=c(0,1250),xlab="number of breakpoints", ylab="matches to predicted terminators",
     axes=F,cex.lab=2)
axis(1,at=seq(0,4000,by=1000),cex.axis=1.6,lwd=3)
axis(2,at=seq(0,1250,by=250),cex.axis=1.6,lwd=3)

lines(cumsum(terminators.info$decafs), col="red",lty=1, lwd=4)
lines(cumsum(terminators.info$hmm), col="black",lty=2, lwd=4)
lines(cumsum(terminators.info$hmm.ori), col="black",lty=1, lwd=4)
#lines(cumsum(terminators.info$hmm.no), col="orange",lty=3, lwd=4)
#lines(cumsum(terminators.info$diff), col="blue",lty=3, lwd=4)
lines(c(0, 8000), c(0, 8000), lty=3, lwd=1, col="darkgreen")
#lines(c(0, 6000), c(1000, 1000), lty=3, lwd=1, col="darkgreen")
#lines(c(6000, 6000), c(0, 1000), lty=3, lwd=1, col="darkgreen")
#legend("bottomright", lty=c(1, 2, 1, 3, 3), col=c("red", "black", "black", "orange", "blue"), legend=c("decafs", "hmmTiling.all", "hmmTiling.ori", "hmmTiling.no", "raw.diff"), lwd=2, cex=4)
legend("topleft", lty=c(1, 2, 1), col=c("red", "black", "black"), legend=c("DeCAFS", "hmmTiling.all", "hmmTiling.ori"), lwd=2, cex=4) #, "hmmTiling.no", "orange"
dev.off()






###############################################################
### plot residuas...
###############################################################
#beta.cv <- betas[apply(TPR[[750]], 2, which.max)][c(1, 3, 5, 7)]
#beta.name <- c("Prom.M", "Prom.P", "Term.M", "Term.P")
# take beta.cv=8 (Learn-Minus Test Plus for Prom)
load("saveRData_rev1/y_Plus_beta=8_default.RData")

n  <- length(res.y$data)
phi <- res.y$modelParameters$phi
er <- res.y$data-res.y$signal
diff_phi <- res.y$signal[-1] - phi*res.y$signal[-n]
residuals <- er[-1] - phi*er[-n]
sd_ <- res.y$modelParameters$sdNu


png("img/7-Some_views.png", width=800, height=600)
par(mfrow=c(2, 3))
plot(res.y$signal, er, pch=20, xlab="Pred.", ylab="Error", main="data minus estimated signal");abline(h=c(-2*sd_, 0, 2*sd_), col="red")

acf(er, main="Acf on data minus estimated signal")
qqnorm(er, main="qqnorm on data minus estimated signal");abline(a=0, b=1, col="red")
plot(diff_phi, residuals, pch=20, xlab="pred.", ylab="Error", main="AR-residuals");abline(h=c(-2*sd_, 0, 2*sd_), col="red")

acf(residuals, main="Acf on AR-residuals")
qqnorm(residuals, main="qqnorm on AR-residuals");abline(a=0, b=1, col="red")
dev.off()
